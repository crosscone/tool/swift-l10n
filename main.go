package main

import (
	"bufio"
	"fmt"
	"os"
)

type token struct {
	key         string
	value       string
	paramName   string
	paramsCount int
}

func main() {
	if len(os.Args) < 3 {
		panic("usage:\nswift-l10n <imput_file> <output_file>")
	}

	inputPath := os.Args[1]
	ouptutPath := os.Args[2]

	inputStrings, err := readLines(inputPath)
	if err != nil {
		panic(err.Error())
	}

	tokens := parseStrings(inputStrings)
	swiftCode := formatTokens(tokens)

	writeString(swiftCode, ouptutPath)
}

func readLines(path string) ([]string, error) {
	file, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer file.Close()

	var lines []string
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		lines = append(lines, scanner.Text())
	}
	return lines, scanner.Err()
}

func writeString(str string, path string) error {
	file, err := os.Create(path)
	if err != nil {
		return err
	}
	defer file.Close()

	w := bufio.NewWriter(file)
	fmt.Fprint(w, str)

	return w.Flush()
}
