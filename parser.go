package main

import "strings"

var globalInput string

func parseStrings(input []string) []token {
	var tokens = []token{}

	for _, inputString := range input {
		token, ok := parseString(inputString)
		if ok {
			tokens = append(tokens, token)
		}
	}

	return tokens
}

func parseString(input string) (token, bool) {
	globalInput = input

	const (
		ParseModeKey     = 0
		ParseModePadding = 1
		ParseModeValue   = 2
		ParseModeStop    = 3
	)

	var inputKey string
	var inputValue string

	parseMode := ParseModeKey

	for _, char := range input {
		if parseMode == ParseModeStop {
			break
		}

		switch parseMode {
		case ParseModeKey:
			if len(inputKey) == 0 && char == '"' {
				continue
			} else if len(inputKey) > 0 && char == '"' {
				parseMode = ParseModePadding
				continue
			} else {
				inputKey += string(char)
			}
		case ParseModePadding:
			if char == '"' {
				parseMode = ParseModeValue
			}
		case ParseModeValue:
			if char == '"' {
				parseMode = ParseModeStop
			} else {
				inputValue += string(char)
			}
		}
	}

	if len(inputKey) > 0 && len(inputValue) > 0 {
		return token{
			key:         inputKey,
			value:       inputValue,
			paramsCount: strings.Count(inputValue, "%@"),
		}, true
	}

	return token{}, false
}
